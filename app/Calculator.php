<?php
namespace App;

class Calculator {
    public $number1;
    public $number2;
    
    function __construct($num1, $num2) {
        $this->number1 = $num1;
        $this->number2 = $num2;
    }

    public function addition() {
        return $this->number1 + $this->number2;
    }

    public function substraction() {
        return $this->number1 - $this->number2;
    }

    public function multiplication() {
        return $this->number1 * $this->number2;
    }

    public function division() {
        return $this->number1 / $this->number2;
    }
}